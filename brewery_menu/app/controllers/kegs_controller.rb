class KegsController < ApplicationController
  before_action :set_keg, only: [:show, :update, :destroy]

  # GET /kegs
  def index
    @kegs = Keg.all

    render json: @kegs
  end

  # GET /kegs/1
  def show
    render json: @keg
  end

  # POST /kegs
  def create
    @keg = Keg.new(keg_params)

    if @keg.save
      render json: @keg, status: :created, location: @keg
    else
      render json: @keg.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /kegs/1
  def update
    if @keg.update(keg_params)
      render json: @keg
    else
      render json: @keg.errors, status: :unprocessable_entity
    end
  end

  # DELETE /kegs/1
  def destroy
    @keg.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_keg
      @keg = Keg.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def keg_params
      params.require(:keg).permit(:beverage_id, :keg_state_id, :keg_size_id)
    end
end
