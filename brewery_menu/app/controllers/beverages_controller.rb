class BeveragesController < ApplicationController
  before_action :set_beverage, only: [:show, :update, :destroy]

  # GET /beverages
  def index
    @beverages = Beverage.all

    render json: @beverages
  end

  def enhancedBeverages
    @beverages = Beverage.all

    bevs = {
        beverages: @beverages.as_json(
            :include => [:beverage_type,
                         :producer,
                         :pour_prices => {
                             :include => [:pour_size],
                             :except => [:pour_size_id, :beverage_id]
                         }],
            :except => [:beverage_type_id, :producer_id])
    }

    render json: bevs
  end

  # GET /beverages/1
  def show
    render json: @beverage
  end

  # POST /beverages
  def create
    params = beverage_params

    bev_type = BeverageType.find_by(type_name: params["beverage_type"])
    producer_name = Producer.find_by(name: params["producer_id"])

    params["pour_prices_attributes"] = JSON.parse params['pour_prices_attributes']
    params["beverage_type"] = bev_type
    params["producer_id"] = producer_name.id

    @beverage = Beverage.new(params)

    if @beverage.save
      render json: @beverage, status: :created, location: @beverage
    else
      render json: @beverage.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /beverages/1
  def update
    params = beverage_params

    bev_type = BeverageType.find_by(type_name: params["beverage_type"])
    producer_name = Producer.find_by(name: params["producer_id"])

    params["pour_prices_attributes"] = JSON.parse params['pour_prices_attributes']
    params["beverage_type"] = bev_type
    params["producer_id"] = producer_name.id

    if @beverage.update(params)
      render json: @beverage
    else
      render json: @beverage.errors, status: :unprocessable_entity
    end
  end

  # DELETE /beverages/1
  def destroy
    corresponding_pour_prices = PourPrice.where(beverage_id: @beverage.id)
    corresponding_pour_prices.each do |pp|
      pp.destroy
    end

    corresponding_kegs = Keg.where(beverage_id: @beverage.id)
    corresponding_kegs.each do |k|
      k.destroy
    end

    corresponding_beverage_to_groupings = BeverageToGrouping.where(beverage_id: @beverage.id)
    corresponding_beverage_to_groupings.each do |btg|
      btg.destroy
    end

    @beverage.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_beverage
      @beverage = Beverage.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def beverage_params
      params.permit(:id, :abv, :name, :color, :description, :beverage_type, :producer_id, :pour_prices_attributes, :id)
    end
end
