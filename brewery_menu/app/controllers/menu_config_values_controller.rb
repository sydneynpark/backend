class MenuConfigValuesController < ApplicationController
  before_action :set_menu_config_value, only: [:show, :update, :destroy]

  # GET /menu_config_values
  def index
    @menu_config_values = MenuConfigValue.all

    render json: @menu_config_values
  end

  # GET /menu_config_values/1
  def show
    render json: @menu_config_value
  end

  # POST /menu_config_values
  def create
    @menu_config_value = MenuConfigValue.new(menu_config_value_params)

    if @menu_config_value.save
      render json: @menu_config_value, status: :created, location: @menu_config_value
    else
      render json: @menu_config_value.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /menu_config_values/1
  def update
    if @menu_config_value.update(menu_config_value_params)
      render json: @menu_config_value
    else
      render json: @menu_config_value.errors, status: :unprocessable_entity
    end
  end

  # DELETE /menu_config_values/1
  def destroy
    @menu_config_value.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_menu_config_value
      @menu_config_value = MenuConfigValue.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def menu_config_value_params
      params.require(:menu_config_value).permit(:menu_config_id, :menu_config_option_id, :value)
    end
end
