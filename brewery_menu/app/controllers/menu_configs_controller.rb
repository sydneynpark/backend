class MenuConfigsController < ApplicationController
  before_action :set_menu_config, only: [:show, :update, :destroy, :show_with_values]

  # GET /menu_configs
  def index
    @menu_configs = MenuConfig.all

    render json: @menu_configs
  end

  # GET /menu_configs/with_values
  def index_with_values
    @menu_configs = MenuConfig.all

    configs_with_details = []
    @menu_configs.each do |one_config|

      one_config_options = MenuConfigOption.get_values_and_defaults one_config.id

      configs_with_details.append({
        menu_config_data: one_config,
        menu_config_options: one_config_options
      })

    end

    render json: configs_with_details
  end

  # GET /menu_configs/1
  def show
    render json: @menu_config
  end

  # GET /menu_configs/with_values/1
  def show_with_values
    options_and_values = MenuConfigOption.get_values_and_defaults @menu_config.id

    render json: {
        menu_config_data: @menu_config,
        menu_config_options: options_and_values
    }
  end

  # POST /menu_configs
  def create

    @menu_config = MenuConfig.new(menu_config_params.except(:values))

    if @menu_config.save

      # Now save the MenuConfigValue objects that are related to this MenuConfig
      option_values = JSON.parse menu_config_params[:values]
      if create_config_values @menu_config.id, option_values
        render json: @menu_config, status: :created, location: @menu_config
      else
        render json: @menu_config.errors, status: :unprocessable_entity
      end

    else
      render json: @menu_config.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /menu_configs/1
  def update

    option_values = JSON.parse menu_config_params[:values]

    if @menu_config.update(menu_config_params.except(:values))

      if update_config_values @menu_config.id, option_values
        render json: @menu_config
      else
        render json: @menu_config.errors, status: :unprocessable_entity
      end
    else
      render json: @menu_config.errors, status: :unprocessable_entity
    end
  end

  # DELETE /menu_configs/1
  def destroy

    related_config_values = MenuConfigValue.where(menu_config_id: @menu_config.id)
    related_config_values.each do |related_config_value|
      related_config_value.destroy
    end

    menus_using_config = Menu.where(menu_config_id: @menu_config.id)
    menus_using_config.each do |menu_using_config|
      menu_using_config.destroy
    end

    @menu_config.destroy

    render json: @menu_config
  end

  private

    # Take a list of Menu Config Values and create them for a given Menu Config
    def create_config_values(menu_config_id, config_value_list)
      success = true

      config_value_list.each do |config_value|
        config_value["menu_config_id"] = menu_config_id
        config_value_object = MenuConfigValue.new(config_value)
        if !config_value_object.save
          success = false
        end
      end

      return success
    end

  def update_config_values(menu_config_id, config_value_list)
    success = true

    config_value_list.each do |config_value|

      # Add the missing piece of information
      config_value['menu_config_id'] = menu_config_id

      # Find the option if it already exists.
      # If `find_by` doesn't fnd a match, it returns nil. Nil evaluates to false in an if check.
      config_value_object = MenuConfigValue.find_by(
          menu_config_id: menu_config_id,
          menu_config_option_id: config_value['menu_config_option_id']
      )

      if config_value_object
        # We found something. Update it before we save.
        config_value_object['value'] = config_value['value']
      else
        # We didn't find anything. Create a new one.
        config_value_object = MenuConfigValue.new(config_value)
      end

      if !config_value_object.save
        success = false
      end

    end

    return success

  end

    # Use callbacks to share common setup or constraints between actions.
    def set_menu_config
      @menu_config = MenuConfig.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def menu_config_params
      params.permit(:id, :name, :description, :values)
    end
end
