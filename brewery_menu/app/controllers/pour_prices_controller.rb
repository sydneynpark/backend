class PourPricesController < ApplicationController
  before_action :set_pour_price, only: [:show, :update, :destroy]

  # GET /pour_prices
  def index
    @pour_prices = PourPrice.all

    render json: @pour_prices
  end

  #GET /pour_prices/prices
  def prices
    @prices = PourPrice.distinct.pluck(:pour_price)

    render json: @prices
  end

  # GET /pour_prices/1
  def show
    render json: @pour_price
  end

  # POST /pour_prices
  def create
    @pour_price = PourPrice.new(pour_price_params)

    if @pour_price.save
      render json: @pour_price, status: :created, location: @pour_price
    else
      render json: @pour_price.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /pour_prices/1
  def update
    if @pour_price.update(pour_price_params)
      render json: @pour_price
    else
      render json: @pour_price.errors, status: :unprocessable_entity
    end
  end

  # DELETE /pour_prices/1
  def destroy
    @pour_price.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pour_price
      @pour_price = PourPrice.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def pour_price_params
      params.permit(:beverage_id, :pour_size_id, :pour_price)
    end
end
