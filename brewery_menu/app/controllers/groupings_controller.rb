 class GroupingsController < ApplicationController
  before_action :set_grouping, only: [:update, :destroy]

  # GET /groupings
  def index
    @groupings = Grouping.all

    render json: @groupings
  end

  # GET /groupings/1
  # if the grouping does not exist return the first 8 beverages in the database instead
  def show
    begin
      @grouping = Grouping.find(params[:id])
      render json: @grouping
    rescue ActiveRecord::RecordNotFound
      # get the first 8 beverages and load the pour prices and beverage types for each
      @beverages = Beverage.includes(:beverage_type, pour_prices: [:pour_size]).first(8)

      #return th beverages and include all of their relationships, exclude the redundant information
      render json: @beverages.to_json(
          :include => [:beverage_type,
                       :pour_prices => {
                           :include => [:pour_size],
                           :except => [:pour_size_id, :beverage_id]
                       }],
          :except => [:beverage_type_id]
      ), status: 404
    end
  end

  # POST /groupings
  def create
    @grouping = Grouping.new(grouping_params)

    if @grouping.save
      render json: @grouping, status: :created, location: @grouping
    else
      render json: @grouping.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /groupings/1
  def update
    if @grouping.update(grouping_params)
      render json: @grouping
    else
      render json: @grouping.errors, status: :unprocessable_entity
    end
  end

  # DELETE /groupings/1
  def destroy
    @grouping.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_grouping
      @grouping = Grouping.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def grouping_params
      params.permit(:name)
    end
end
