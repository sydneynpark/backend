class ProducersController < ApplicationController
  before_action :set_producer, only: [:show, :update, :destroy]

  # GET /producers
  def index
    @producers = Producer.all

    render json: @producers
  end

  # GET /producers/1
  def show
    render json: @producer
  end

  # POST /producers
  def create
    @producer = Producer.new(producer_params)

    if @producer.save
      render json: @producer, status: :created, location: @producer
    else
      render json: @producer.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /producers/1
  def update
    if @pour_size.update(pour_size_params)
      render json: @pour_size
    else
      render json: @pour_size.errors, status: :unprocessable_entity
    end
  end

  # DELETE /producers/1
  def destroy
    @producer.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_producer
      @producer = Producer.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def producer_params
      params.permit(:name, :description)
    end
end
