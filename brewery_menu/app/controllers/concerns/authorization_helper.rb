module AuthorizationHelper
  $url = URI("https://www.googleapis.com/oauth2/v4/token")
  require 'net/http'
  include EncryptionHelper

  # access_token    A token that can be sent to a Google API.
  # id_token        A JWT that contains identity information about the user that is digitally signed by Google.
  # expires_in      The remaining lifetime of the access token.
  # token_type      Identifies the type of token returned. At this time, this field always has the value Bearer.
  # refresh_token   (optional)	This field is only present if access_type=offline is included in the authentication request. For details, see Refresh tokens.
  def AuthorizationHelper.getUserFromCode(code)

    params = {
      "code" => code,
      "client_id" => ENV['GOOGLE_CLIENT_ID'],
      "client_secret" => ENV['GOOGLE_CLIENT_SECRET'],
      "grant_type" => 'authorization_code',
      "redirect_uri" => ENV['REDIRECT_URI'],
    }

    response =  Net::HTTP.post_form($url, params)

    return JSON.parse(response.read_body)
  end
end
