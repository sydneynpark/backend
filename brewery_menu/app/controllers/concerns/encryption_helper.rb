module EncryptionHelper
  require 'openssl'
  @decipher = OpenSSL::Cipher::AES.new(128, :CBC)

  def EncryptionHelper.decrypt(encrypted)
    puts "decrypting"
    puts OpenSSL::Random.random_bytes(16)

    @decipher.decrypt
    @decipher.key = ['c27e168bb78f750f93555484c9c03877'].pack('H*')
    @decipher.iv = ['00000000000000000000000000000000'].pack('H*')
    @decipher.padding = 0

    plain = @decipher.update(['29b046549bdb03e1bf01a32630dda62ab85223d153ecd29d69c8926e075b660a'].pack('H*')) + @decipher.final
    puts "output"
    puts plain.unpack('H*')
    return plain
  end

end
