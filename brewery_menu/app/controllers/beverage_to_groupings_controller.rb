class BeverageToGroupingsController < ApplicationController
  before_action :set_beverage_to_grouping, only: [:show, :update, :destroy]

  # GET /beverage_to_groupings/1
  def index
    @beverage_to_groupings = BeverageToGrouping.all

    render json: @beverage_to_groupings
  end

  # GET /beverage_to_groupings/0
  def show
    render json: @beverage_to_grouping
  end

  # POST /beverage_to_groupings
  def create
    params = beverage_to_grouping_params

    beverage = Beverage.find_by(id: params["beverage_id"])
    grouping = Grouping.find_by(id: params["grouping_id"])

    params["beverage_id"] = beverage.id
    params["grouping_id"] = grouping.id

    @beverage_to_grouping = BeverageToGrouping.new(params)

    if @beverage_to_grouping.save
      render json: @beverage_to_grouping, status: :created, location: @beverage
    else
      render json: @beverage_to_grouping.errors, status: :unprocessable_entity
    end
  end

  # POST /beverage_to_groupings
  # def create
  #   @beverage_to_grouping = BeverageToGrouping.new(beverage_type_grouping_params)
  #
  #   if @beverage_to_grouping.save
  #     render json: @beverage_to_grouping, status: :created, location: @beverage_to_grouping
  #   else
  #     render json: @beverage_to_grouping.errors, status: :unprocessable_entity
  #   end
  # end

  # PATCH/PUT /beverage_to_groupings/1
  def update
    if @beverage_to_grouping.update(beverage_type_grouping_params)
      render json: @beverage_to_grouping
    else
      render json: @beverage_to_grouping.errors, status: :unprocessable_entity
    end
  end

  # DELETE /beverage_to_groupings/1
  def destroy
    @beverage_to_grouping.destroy
  end

  # GET /bev_names_for_group/1
  def getBevNamesForGroup
      # @beverages_ids = BeverageToGrouping.find_by grouping: params[:group]
      # @beverages = Beverage.find_by id: @beverages_ids.beverage
      # @beverages = Beverage.joins(articles: [beverages_ids:beverage])
      @beverages = BeverageToGrouping.joins(:beverage)
      @group_bevs = @beverages.where grouping_id: params[:group]
      # @beverages_result = Beverage.joins(group_bevs:beverage_id)

      render json: @group_bevs.as_json(:include => [:beverage])

  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_beverage_to_grouping
      @beverage_to_grouping = BeverageToGrouping.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def beverage_to_grouping_params
      params.permit(:beverage_id, :grouping_id)
    end
end
