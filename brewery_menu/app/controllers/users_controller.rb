require 'jwt'
class UsersController < ApplicationController
  include AuthorizationHelper

  # Post: /login
  # params:
  #   access_token: google user offline token to redeem for google id token
  # response:
  #   body: { given_name, family_name, email, sub } status: 200 : when the user id is in the database
  #   body: {} status: 403 : when the user id is not in the database
  def login
    request_body = JSON.parse(request.body.read)

    code =  request_body["access_token"]

    google_response = AuthorizationHelper.getUserFromCode(code)

    user_info = JWT.decode(google_response['id_token'], nil, false)[0]
    user_exists = User.find_by(id: user_info["sub"])

    if user_exists
      render json: {given_name: user_info["given_name"], family_name: user_info["family_name"], email: user_info['email'], sub: user_info['sub']}, status: 200
    else
      render status: 403
    end
  end


  # POST: /create
  # params:
  #   access_token: google user offline token to redeem for google id token
  # response:
  #   body: { given_name, family_name, email, sub } status: 200 : when the user id does not exist in the database and is successfully added
  #   body: { given_name, family_name, email, sub } status: 403 : when the user id is already in the database
  def create
    request_body = JSON.parse(request.body.read)
    code =  request_body["access_token"]

    google_response = AuthorizationHelper.getUserFromCode(code)

    user_info = JWT.decode(google_response['id_token'], nil, false)[0]

    @user = User.find_by(id: user_info['sub'])

    if @user == nil
      @user = User.new(id: user_info['sub'], token: google_response['refresh_token'])
      if @user.save
        render json: {given_name: user_info["given_name"], family_name: user_info["family_name"], email: user_info['email'], sub: user_info['sub']}, status: 200
      else
        render status: 500
      end
    else
      puts "user already exists"
      render json: {given_name: user_info["given_name"], family_name: user_info["family_name"], email: user_info['email'], sub: user_info['sub']}, status: 403
    end
  end

  private
    # Only allow a trusted parameter "white list" through.
    def user_params
      params.require(:access_token)
    end
end
