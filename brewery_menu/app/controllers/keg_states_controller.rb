class KegStatesController < ApplicationController
  before_action :set_keg_state, only: [:show, :update, :destroy]

  # GET /keg_states
  def index
    @keg_states = KegState.all

    render json: @keg_states
  end

  # GET /keg_states/1
  def show
    render json: @keg_state
  end

  # POST /keg_states
  def create
    @keg_state = KegState.new(keg_state_params)

    if @keg_state.save
      render json: @keg_state, status: :created, location: @keg_state
    else
      render json: @keg_state.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /keg_states/1
  def update
    if @keg_state.update(keg_state_params)
      render json: @keg_state
    else
      render json: @keg_state.errors, status: :unprocessable_entity
    end
  end

  # DELETE /keg_states/1
  def destroy
    @keg_state.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_keg_state
      @keg_state = KegState.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def keg_state_params
      params.permit(:description)
    end
end
