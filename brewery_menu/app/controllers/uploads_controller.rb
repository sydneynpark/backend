class UploadsController < ApplicationController

	DATABASE_EXPORT_ORDER = [
			User,               # Dump this separately because it needs custom Excludes

			MenuConfig,
			MenuConfigOption,     # depends on: MenuConfig
			MenuConfigValue,      # depends on: MenuConfig, MenuConfigOption

			Producer,
			BeverageType,
			Beverage,             # depends on: BeverageType, Producer

			PourSize,
			PourPrice,            # depends on: Beverage, PourSize

			Grouping,
			BeverageToGrouping,   # depends on: Beverage, Grouping

			Menu,                 # depends on: MenuConfig, Grouping

			KegSize,
			KegState,
			Keg                   # depends on: KegSize, KegState, Beverage
	]

	def parseFile
		file = params["file"]

		# Delete all of the data before we load new data in
		DATABASE_EXPORT_ORDER.reverse.each do |model|
			model.delete_all
		end

		# Run the seeds file generated to load new data
		exec `bundle exec rails runner #{file.path}`

		# Reset the primary keys on all of the tables
		ActiveRecord::Base.connection.tables.each do |table|
			ActiveRecord::Base.connection.reset_pk_sequence!(table)
		end

	end

	private
		def upload_params
	      params.permit(:file)
	    end
end
