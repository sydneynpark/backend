class MenuConfigOptionsController < ApplicationController
  before_action :set_menu_config_option, only: [:show, :update, :destroy]

  # GET /menu_config_options
  def index
    @menu_config_options = MenuConfigOption.all

    render json: @menu_config_options
  end

  # GET /menu_config_options/1
  def show
    render json: @menu_config_option
  end

  # POST /menu_config_options
  def create
    @menu_config_option = MenuConfigOption.new(menu_config_option_params)

    if @menu_config_option.save
      render json: @menu_config_option, status: :created, location: @menu_config_option
    else
      render json: @menu_config_option.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /menu_config_options/1
  def update
    if @menu_config_option.update(menu_config_option_params)
      render json: @menu_config_option
    else
      render json: @menu_config_option.errors, status: :unprocessable_entity
    end
  end

  # DELETE /menu_config_options/1
  def destroy
    @menu_config_option.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_menu_config_option
      @menu_config_option = MenuConfigOption.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def menu_config_option_params
      params.require(:menu_config_option).permit(:name, :datatype, :default_value)
    end
end
