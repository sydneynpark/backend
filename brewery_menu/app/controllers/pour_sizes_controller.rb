class PourSizesController < ApplicationController
  before_action :set_pour_size, only: [:show, :update, :destroy]

  # GET /pour_sizes
  def index
    @pour_sizes = PourSize.all

    render json: @pour_sizes
  end

  # GET /pour_sizes/1
  def show
    render json: @pour_size
  end

  # POST /pour_sizes
  def create
    @pour_size = PourSize.new(pour_size_params)

    if @pour_size.save
      render json: @pour_size, status: :created, location: @pour_size
    else
      render json: @pour_size.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /pour_sizes/1
  def update
    if @pour_size.update(pour_size_params)
      render json: @pour_size
    else
      render json: @pour_size.errors, status: :unprocessable_entity
    end
  end

  # DELETE /pour_sizes/1
  def destroy
    @pour_size.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_pour_size
      @pour_size = PourSize.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def pour_size_params
      # All write requests MUST have the size attribute.
      params.require(:pour_size).permit(:size)
    end
end
