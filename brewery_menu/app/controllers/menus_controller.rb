class MenusController < ApplicationController
  before_action :set_menu, only: [ :update, :destroy, :show]

  # GET /menus
  def index
    @menus = Menu.all

    render json: @menus
  end

  # GET /menus/1
  def show

    values_and_defaults = MenuConfigOption.get_values_and_defaults @menu.menu_config_id

    # get the beverages for the menu
    if @menu.grouping.name == 'default'
      #get the first 8 beverages
      beverages = Beverage.includes(:beverage_type, :producer, pour_prices: [:pour_size]).first(8)
    else
      beverages = @menu.grouping.beverages
    end

    # generate the returned object. the as_json line indicates what relationships to include
    menu_to_return = {
        menu_config: values_and_defaults,
        beverages: beverages.as_json(
            :include => [:beverage_type,
                         :producer,
                         :pour_prices => {
                             :include => [:pour_size],
                             :except => [:pour_size_id, :beverage_id]
                         }],
            :except => [:beverage_type_id, :producer_id])
    }

    render json: menu_to_return
  end

  # POST /menus
  def create
    @menu = Menu.find_by(menu_params)
    if @menu
      @save_success = true
    else
      @menu = Menu.new(menu_params)
      @save_success = @menu.save
    end


    if @save_success
      render json: @menu, status: :created, location: @menu
    else
      render json: @menu.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /menus/1
  def update
    if @menu.update(menu_params)
      render json: @menu
    else
      render json: @menu.errors, status: :unprocessable_entity
    end
  end

  # DELETE /menus/1
  def destroy
    @menu.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_menu
      @menu = Menu.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def menu_params
      params.require(:menu).permit(:menu_config_id, :grouping_id)
    end
end
