class KegSizesController < ApplicationController
  before_action :set_keg_size, only: [:show, :update, :destroy]

  # GET /keg_sizes
  def index
    @keg_sizes = KegSize.all

    render json: @keg_sizes
  end

  # GET /keg_sizes/1
  def show
    render json: @keg_size
  end

  # POST /keg_sizes
  def create
    @keg_size = KegSize.new(keg_size_params)

    if @keg_size.save
      render json: @keg_size, status: :created, location: @keg_size
    else
      render json: @keg_size.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /keg_sizes/1
  def update
    if @keg_size.update(keg_size_params)
      render json: @keg_size
    else
      render json: @keg_size.errors, status: :unprocessable_entity
    end
  end

  # DELETE /keg_sizes/1
  def destroy
    @keg_size.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_keg_size
      @keg_size = KegSize.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def keg_size_params
      params.permit(:name)
    end
end
