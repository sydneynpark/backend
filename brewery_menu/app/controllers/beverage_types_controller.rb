class BeverageTypesController < ApplicationController
  before_action :set_beverage_type, only: [:show, :update, :destroy]

  # GET /beverage_types
  def index
    @beverage_types = BeverageType.all

    render json: @beverage_types
  end

  # GET /beverage_types/1
  def show
    render json: @beverage_type
  end

  # POST /beverage_types
  def create
    @beverage_type = BeverageType.new(beverage_type_params)

    if @beverage_type.save
      render json: @beverage_type, status: :created, location: @beverage_type
    else
      render json: @beverage_type.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /beverage_types/1
  def update
    if @beverage_type.update(beverage_type_params)
      render json: @beverage_type
    else
      render json: @beverage_type.errors, status: :unprocessable_entity
    end
  end

  # DELETE /beverage_types/1
  def destroy
    @beverage_type.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_beverage_type
      @beverage_type = BeverageType.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def beverage_type_params
      params.permit(:type_name,:description)
    end
end
