require 'tempfile'

class BackupController < ApplicationController

  DATABASE_EXPORT_ORDER = [
      # User,               # Dump this separately because it needs custom Excludes

      MenuConfig,
      MenuConfigOption,     # depends on: MenuConfig
      MenuConfigValue,      # depends on: MenuConfig, MenuConfigOption

      Producer,
      BeverageType,
      Beverage,             # depends on: BeverageType, Producer

      PourSize,
      PourPrice,            # depends on: Beverage, PourSize

      Grouping,
      BeverageToGrouping,   # depends on: Beverage, Grouping

      Menu,                 # depends on: MenuConfig, Grouping

      KegSize,
      KegState,
      Keg                   # depends on: KegSize, KegState, Beverage
  ]


  def create

    # Name the file using the current datetime so the user can tell their dumps apart
    now_string = Time.now.strftime('%Y%m%d_%k%M%S')
    seed_dump_filename = 'brewerymenubackup_%s.rb' % now_string

    # Create a temp file so rails can manage the deletion of it when we are finished using it.
    dump_file = Tempfile.new(seed_dump_filename)

    # First dump all of the Users to the dump file, skipping over their tokens so we respect security
    SeedDump.dump(User, file: dump_file, append: true, exclude: [:token])

    # Then dump the actual data that we care about.
    DATABASE_EXPORT_ORDER.each do |model|
      SeedDump.dump(model, file: dump_file, append: true, exclude: [])
    end

    # Send the file so it pops up in the downloads
    send_file(dump_file, filename: seed_dump_filename)

    # Indicate that we are finished using the temp file
    dump_file.close
  end

  def load
  end

  private

  # Use callbacks to share common setup or constraints between actions.
  def set_requester
  end

  # Only allow a trusted parameter "white list" through.
  def menu_params
  end

end
