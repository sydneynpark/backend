class ApplicationController < ActionController::API
  include Response

  rescue_from ActionController::ParameterMissing do |exception|
    json_response({ message: exception }, :unprocessable_entity)
  end

  rescue_from ActiveRecord::RecordNotFound do |exception|
    json_response({ message: exception }, :not_found)
  end
end
