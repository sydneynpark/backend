class MenuConfigValue < ApplicationRecord
  belongs_to :menu_config
  belongs_to :menu_config_option
end
