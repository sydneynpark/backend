class MenuConfigOption < ApplicationRecord
  has_many :menu_config_values

  def self.get_values_and_defaults(config_id)
    #select the menu_config_values associated with the specified menu_config
    values = MenuConfigValue.select("value, menu_config_option_id")
                 .where(menu_config_id: config_id)

    #join the list of values and options
    values_and_defaults = MenuConfigOption.joins("left join (#{values.to_sql}) as values
                                     on menu_config_options.id = values.menu_config_option_id")
                              .select("menu_config_options.*, values.value")

    return values_and_defaults
  end
end
