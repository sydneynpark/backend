class Keg < ApplicationRecord
  belongs_to :beverage
  belongs_to :keg_state
  belongs_to :keg_size
end
