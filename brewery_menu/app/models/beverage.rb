class Beverage < ApplicationRecord
  belongs_to :beverage_type
  belongs_to :producer
  has_many :beverage_to_groupings
  has_many :groupings, through: :beverage_to_groupings
  has_many :pour_prices, :autosave => true
  has_many :pour_sizes, through: :pour_prices

  accepts_nested_attributes_for :pour_prices
end
