class BeverageToGrouping < ApplicationRecord
  belongs_to :beverage
  belongs_to :grouping
end
