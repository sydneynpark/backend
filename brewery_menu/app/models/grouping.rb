class Grouping < ApplicationRecord
    has_many :beverage_to_groupings
    has_many :beverages, through: :beverage_to_groupings
end
