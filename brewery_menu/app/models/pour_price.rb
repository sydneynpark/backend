class PourPrice < ApplicationRecord
  belongs_to :beverage
  belongs_to :pour_size, :autosave => true

  accepts_nested_attributes_for :pour_size
end
