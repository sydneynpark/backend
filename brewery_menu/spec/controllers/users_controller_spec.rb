require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  # Store a user in the database with the provided ID and token
  def create_known_user known_id, known_token
    known_user = User.new
    known_user.id = known_id
    known_user.token = known_token
    known_user.save
  end

  # Mock out the HTTP request to google and build a mock response to return instead whenever it is called
  def prepare_http_response user_id, refresh_token_res: "ABC",
                            user_first_name: "Victor", user_last_name: "Frankenstein", user_email: "ahops@msoe.edu"

    # Prepare the contents of the id_token based on the information provided
    user_info = {
        "sub"         => user_id,
        "given_name"  => user_first_name,
        "family_name" => user_last_name,
        "email"       => user_email,
    }

    # Build a token from the user info provided
    id_token_res = JWT.encode(user_info, nil, 'none') # TODO find how to encode without creating a null jwt

    response_body = {
        "id_token"        => id_token_res,
        "refresh_token"   => refresh_token_res
    }

    # Return the prepared response
    allow(AuthorizationHelper).to receive(:getUserFromCode).and_return(response_body)
  end


  before do
    # Do nothing
  end

  after do
    # Do nothing
  end

  context 'when creating a user that already exists' do
    it 'should return a 403' do
      # allow(AuthorizationHelper).to receive(:getUserFromCode).and_return("MOCK1")
      # result = AuthorizationHelper.getUserFromCode("ABC")
      # expect(result).to eq("MOCK1")


      TEST_ID = "ExistingUserSignUpId"
      TEST_TOKEN = "ExistingUserSignUpToken"

      create_known_user TEST_ID, TEST_TOKEN
      prepare_http_response TEST_ID, refresh_token_res: TEST_TOKEN

      post "create", params: {
          # id: TEST_ID,
          access_token: TEST_TOKEN
      }, as: :json

      expect(response.status).to eq(403)

    end
  end

  context 'when creating a new user' do
    it 'should return 200 with the correct user information' do
      TEST_ID = "NewUserSignUpId"
      TEST_TOKEN = "NewUserSignUpToken"

      TEST_FIRST_NAME = "New"
      TEST_LAST_NAME = "User"
      TEST_EMAIL = "new_user@msoe.edu"

      prepare_http_response TEST_ID,
                            refresh_token_res: TEST_TOKEN,
                            user_first_name: TEST_FIRST_NAME,
                            user_last_name: TEST_LAST_NAME,
                            user_email: TEST_EMAIL

      post "create", params: {
          # id: TEST_ID,
          access_token: TEST_TOKEN
      }, as: :json

      expect(response.status).to eq(200)

      body = JSON.parse(response.body)
      expect(body["given_name"]).to eq(TEST_FIRST_NAME)
      expect(body["family_name"]).to eq(TEST_LAST_NAME)
      expect(body["email"]).to eq(TEST_EMAIL)
      expect(body["sub"]).to eq(TEST_ID)
    end
  end

  context 'when signing in with correct credentials' do
    it 'should return 200 with the correct user information' do
      TEST_ID = "ExistingUserLoginId"
      TEST_TOKEN = "ExistingUserLoginToken"

      TEST_FIRST_NAME = "User"
      TEST_LAST_NAME = "WhoExists"
      TEST_EMAIL = "existingUser@msoe.edu"

      create_known_user TEST_ID, TEST_TOKEN
      prepare_http_response TEST_ID,
                            refresh_token_res: TEST_TOKEN,
                            user_first_name: TEST_FIRST_NAME,
                            user_last_name: TEST_LAST_NAME,
                            user_email: TEST_EMAIL

      post "login", params: {
          # id: TEST_ID,
          access_token: TEST_TOKEN
      }, as: :json

      expect(response.status).to eq(200)

      body = JSON.parse(response.body)
      expect(body["given_name"]).to eq(TEST_FIRST_NAME)
      expect(body["family_name"]).to eq(TEST_LAST_NAME)
      expect(body["email"]).to eq(TEST_EMAIL)
      expect(body["sub"]).to eq(TEST_ID)

    end
  end

  context 'when signing into a user that doesn\'t exist' do
    it 'should return 403' do
      TEST_ID = "NonExistingUserId"
      TEST_TOKEN = "NonExistingingUserToken"

      prepare_http_response TEST_ID, refresh_token_res: TEST_TOKEN

      post "login", params: {
          access_token: TEST_TOKEN
      }, as: :json

      expect(response.status).to eq(403)
    end
  end



end
