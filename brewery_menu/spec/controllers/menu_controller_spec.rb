require 'rails_helper'

RSpec.describe MenusController, type: :controller do

  def create_beverage_type
    beverage_type = BeverageType.new
    beverage_type.id = 1
    beverage_type.type_name = "test type"
    beverage_type.description = "test type description "
    beverage_type.save
  end

  def create_producer
    producer = Producer.new
    producer.id = 1
    producer.name = 'producer'
    producer.description = 'producer description'
    producer.save
  end

  def create_pour_size
    pour_size = PourSize.new
    pour_size.id = 1
    pour_size.size = "3oz"
    pour_size.save
  end

  def create_beverage
    beverage = Beverage.new
    beverage.id = 1
    beverage.abv = 4.5
    beverage.name = "Beer 1"
    beverage.color = "#a66f0a"
    beverage.description = "This is Beer 1, I am not very creative"
    beverage.beverage_type_id = 1
    beverage.producer_id= 1
    beverage.save
  end

  def create_pour_price
    pour_price = PourPrice.new
    pour_price.id = 1
    pour_price.beverage_id = 1
    pour_price.pour_size_id = 1
    pour_price.pour_price = 3.0
    pour_price.save
  end

  def create_grouping
    grouping = Grouping.new
    grouping.id = 1
    grouping.name = 'default'
    grouping.save
  end

  def create_menu_config
    menu_config = MenuConfig.new
    menu_config.id = 1
    menu_config.name = 'test'
    menu_config.description = 'config description'
    menu_config.save
  end

  def create_menu_config_options
    menu_config_option_1 = MenuConfigOption.new
    menu_config_option_1.id = 1
    menu_config_option_1.name = "rows"
    menu_config_option_1.datatype ="integer"
    menu_config_option_1.default_value = "1"
    menu_config_option_1.save

    menu_config_option_2 = MenuConfigOption.new
    menu_config_option_2.id = 2
    menu_config_option_2.name = "cols"
    menu_config_option_2.datatype ="integer"
    menu_config_option_2.default_value = "1"
    menu_config_option_2.save
  end

  def create_menu_config_value
    menu_config_value = MenuConfigValue.new
    menu_config_value.id = 1
    menu_config_value.menu_config_id = 1
    menu_config_value.menu_config_option_id = 1
    menu_config_value.value = "2"
    menu_config_value.save
  end

  def create_menu
    menu = Menu.new
    menu.id = 1
    menu.menu_config_id = 1
    menu.grouping_id = 1
    menu.save
  end

  before do
    create_beverage_type
    create_producer
    create_pour_size
    create_beverage
    create_pour_price
    create_grouping
    create_menu_config
    create_menu_config_options
    create_menu_config_value
    create_menu
  end

  after do
    # Do nothing
  end

  context 'when retrieving a menu' do
    it 'should return 200 with valid id' do
      get 'show', params: {id: 1}
      expect(response.status).to be(200)
    end

    it 'should return 404 with invalud id' do
      get 'show', params: {id: 2}
      expect(response.status).to be(404)
    end
  end
end