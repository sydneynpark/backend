BeverageType.create!([
  {type_name: "IPA", description: "This is an IPA, it doesnt taste very good because it is an IPA"},
  {type_name: "Sour", description: "This is an Sour, the best kind of beer (because it tastes like soda)"}
])
Producer.create!([
    {name: "Guy", description: "That one guy."}
])
Beverage.create!([
  {abv: 4.5, name: "Beer 1", color: "#a66f0a", description: "This is Beer 1, I am not very creative", beverage_type_id: 1, producer_id: 1},
  {abv: 7.5, name: "Beer 2", color: "#e3bb71", description: "This is Beer 2, I am not very creative", beverage_type_id: 1, producer_id: 1},
  {abv: 7.0, name: "Beer 3", color: "#8f5211", description: "This is Beer 3, I am not very creative", beverage_type_id: 1, producer_id: 1},
  {abv: 5.5, name: "Beer 4", color: "#c95908", description: "This is Beer 4, I am not very creative", beverage_type_id: 1, producer_id: 1},
  {abv: 5.5, name: "Beer 5", color: "#d6986b", description: "This is Beer 5, I am not very creative", beverage_type_id: 1, producer_id: 1},
  {abv: 5.0, name: "Beer 7", color: "#ffd085", description: "This is Beer 7, I am not very creative", beverage_type_id: 1, producer_id: 1},
  {abv: 6.0, name: "Beer 6", color: "#e68d00", description: "This is Beer 6, How did this sour get in here", beverage_type_id: 2, producer_id: 1},
])
PourSize.create!([
  {size: "3oz"},
  {size: "6oz"},
  {size: "9oz"}
])
PourPrice.create!([
  {beverage_id: 1, pour_size_id: 1, pour_price: 3.0},
  {beverage_id: 1, pour_size_id: 2, pour_price: 6.0},
  {beverage_id: 1, pour_size_id: 3, pour_price: 9.0},
  {beverage_id: 2, pour_size_id: 1, pour_price: 3.0},
  {beverage_id: 2, pour_size_id: 2, pour_price: 6.0},
  {beverage_id: 2, pour_size_id: 3, pour_price: 9.0},
  {beverage_id: 3, pour_size_id: 1, pour_price: 3.0},
  {beverage_id: 3, pour_size_id: 2, pour_price: 6.0},
  {beverage_id: 3, pour_size_id: 3, pour_price: 9.0},
  {beverage_id: 4, pour_size_id: 1, pour_price: 3.0},
  {beverage_id: 4, pour_size_id: 2, pour_price: 6.0},
  {beverage_id: 4, pour_size_id: 3, pour_price: 9.0},
  {beverage_id: 5, pour_size_id: 1, pour_price: 3.0},
  {beverage_id: 5, pour_size_id: 2, pour_price: 6.0},
  {beverage_id: 5, pour_size_id: 3, pour_price: 9.0},
  {beverage_id: 6, pour_size_id: 1, pour_price: 3.0},
  {beverage_id: 6, pour_size_id: 2, pour_price: 6.0},
  {beverage_id: 6, pour_size_id: 3, pour_price: 9.0},
  {beverage_id: 7, pour_size_id: 1, pour_price: 3.0},
  {beverage_id: 7, pour_size_id: 2, pour_price: 6.0},
  {beverage_id: 7, pour_size_id: 3, pour_price: 9.0},
])
Grouping.create!([
                     {name: "default"}
                 ])
MenuConfig.create!([
                       {name: "Default", description: "This is the default menu config, it will use all of the default values for the configuration options"},
                   ])
Menu.create!([
                 {menu_config_id: 1, grouping_id: 1}
             ])
MenuConfigOption.create!([
                             {name: "display_type", datatype: "display_type", default_value: "grid"},
                             {name: "table_header_color", datatype: "color", default_value: "#2e2e2e"},
                             {name: "rows", datatype: "integer", default_value: "4"},
                             {name: "columns", datatype: "integer", default_value: "2"},
                             {name: "use_background_image", datatype: "boolean", default_value: "false"},
                             {name: "background_image", datatype: "string", default_value: "nil"},
                             {name: "title_font", datatype: "font", default_value: "stentiga"},
                             {name: "text_font", datatype: "font", default_value: "sans-serif"},
                             {name: "title", datatype: "string", default_value: "Test Menu"},
                             {name: "include_description", datatype: "boolean", default_value: "true"},
                             {name: "include_pours", datatype: "boolean", default_value: "true"},
                             {name: "include_producers", datatype: "boolean", default_value: "true"},
                             {name: "background_color", datatype: "color", default_value: "#404040"},
                             {name: "beverage_card_color", datatype: "color", default_value: "#2e2e2e"}
                         ])
MenuConfigValue.create!([
  {menu_config_id: 1, menu_config_option_id: 7, value: "Test Menu"},
])
