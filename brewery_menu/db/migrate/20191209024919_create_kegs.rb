class CreateKegs < ActiveRecord::Migration[6.0]
  def change
    create_table :kegs do |t|
      t.belongs_to :beverage, null: false, foreign_key: true
      t.belongs_to :keg_state, null: false, foreign_key: true
      t.belongs_to :keg_size, null: false, foreign_key: true
    end
  end
end
