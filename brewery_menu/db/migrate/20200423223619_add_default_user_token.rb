class AddDefaultUserToken < ActiveRecord::Migration[6.0]
  def change
    change_column_default(:users, :token, from: nil, to: '')
  end
end
