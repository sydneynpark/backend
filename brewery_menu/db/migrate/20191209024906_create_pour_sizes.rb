class CreatePourSizes < ActiveRecord::Migration[6.0]
  def change
    create_table :pour_sizes do |t|
      t.string :size
    end
  end
end
