class CreateMenuConfigs < ActiveRecord::Migration[6.0]
  def change
    create_table :menu_configs do |t|
      t.string :name
      t.text :description
    end
  end
end
