class CreateBeverageTypes < ActiveRecord::Migration[6.0]
  def change
    create_table :beverage_types do |t|
      t.string :type_name
      t.text :description
    end
  end
end
