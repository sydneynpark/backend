class CreateBeverages < ActiveRecord::Migration[6.0]
  def change
    create_table :beverages do |t|
      t.float :abv
      t.string :name
      t.text :color
      t.text :description
      t.belongs_to :beverage_type, null: false, foreign_key: true
    end
  end
end
