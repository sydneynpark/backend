class UpdateMenuConfigSeeds < ActiveRecord::Migration[6.0]
  def change

    MenuConfigOption.where(name: ['title_font', 'text_font']).each do |config_option|
      config_option.update_attributes!(:datatype => 'font')
    end

    MenuConfigOption.where(name: ['background_color', 'beverage_card_color']).each do |config_option|
      config_option.update_attributes!(:datatype => 'color')
    end

  end
end
