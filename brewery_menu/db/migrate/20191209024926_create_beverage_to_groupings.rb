class CreateBeverageToGroupings < ActiveRecord::Migration[6.0]
  def change
    create_table :beverage_to_groupings do |t|
      t.belongs_to :beverage, null: false, foreign_key: true
      t.belongs_to :grouping, null: false, foreign_key: true
    end
  end
end
