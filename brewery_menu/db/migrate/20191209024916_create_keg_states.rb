class CreateKegStates < ActiveRecord::Migration[6.0]
  def change
    create_table :keg_states do |t|
      t.string :description
    end
  end
end
