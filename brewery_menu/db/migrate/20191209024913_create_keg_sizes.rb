class CreateKegSizes < ActiveRecord::Migration[6.0]
  def change
    create_table :keg_sizes do |t|
      t.string :name
    end
  end
end
