class CreatePourPrices < ActiveRecord::Migration[6.0]
  def change
    create_table :pour_prices do |t|
      t.belongs_to :beverage, null: false, foreign_key: true
      t.belongs_to :pour_size, null: false, foreign_key: true
    end
  end
end
