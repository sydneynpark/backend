class AddIndexToTable < ActiveRecord::Migration
  def change
    add_index :menu_config_values, %w(menu_config menu_config_option), :unique => true
  end
end