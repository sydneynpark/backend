class CreateMenuConfigOptions < ActiveRecord::Migration[6.0]
  def change
    create_table :menu_config_options do |t|
      t.string :name
      t.string :datatype
      t.string :default_value
    end
  end
end
