class AddProducerToBeverages < ActiveRecord::Migration[6.0]
  def change
    add_reference :beverages, :producer, foreign_key: true
  end
end
