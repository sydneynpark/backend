class CreateMenus < ActiveRecord::Migration[6.0]
  def change
    create_table :menus do |t|
      t.belongs_to :menu_config, null: false, foreign_key: true
      t.belongs_to :grouping, null: false, foreign_key: true
    end
  end
end
