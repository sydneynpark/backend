class CreateMenuConfigValues < ActiveRecord::Migration[6.0]
  def change
    create_table :menu_config_values do |t|
      t.belongs_to :menu_config, null: false, foreign_key: true
      t.belongs_to :menu_config_option, null: false, foreign_key: true
      t.string :value
    end
  end
end
