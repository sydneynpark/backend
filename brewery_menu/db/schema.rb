# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_04_23_223619) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "beverage_to_groupings", force: :cascade do |t|
    t.bigint "beverage_id", null: false
    t.bigint "grouping_id", null: false
    t.index ["beverage_id"], name: "index_beverage_to_groupings_on_beverage_id"
    t.index ["grouping_id"], name: "index_beverage_to_groupings_on_grouping_id"
  end

  create_table "beverage_types", force: :cascade do |t|
    t.string "type_name"
    t.text "description"
  end

  create_table "beverages", force: :cascade do |t|
    t.float "abv"
    t.string "name"
    t.text "color"
    t.text "description"
    t.bigint "beverage_type_id", null: false
    t.bigint "producer_id", null: false
    t.index ["beverage_type_id"], name: "index_beverages_on_beverage_type_id"
    t.index ["producer_id"], name: "index_beverages_on_producer_id"
  end

  create_table "groupings", force: :cascade do |t|
    t.string "name"
  end

  create_table "keg_sizes", force: :cascade do |t|
    t.string "name"
  end

  create_table "keg_states", force: :cascade do |t|
    t.string "description"
  end

  create_table "kegs", force: :cascade do |t|
    t.bigint "beverage_id", null: false
    t.bigint "keg_state_id", null: false
    t.bigint "keg_size_id", null: false
    t.index ["beverage_id"], name: "index_kegs_on_beverage_id"
    t.index ["keg_size_id"], name: "index_kegs_on_keg_size_id"
    t.index ["keg_state_id"], name: "index_kegs_on_keg_state_id"
  end

  create_table "menu_config_options", force: :cascade do |t|
    t.string "name"
    t.string "datatype"
    t.string "default_value"
  end

  create_table "menu_config_values", force: :cascade do |t|
    t.bigint "menu_config_id", null: false
    t.bigint "menu_config_option_id", null: false
    t.string "value"
    t.index ["menu_config_id"], name: "index_menu_config_values_on_menu_config_id"
    t.index ["menu_config_option_id"], name: "index_menu_config_values_on_menu_config_option_id"
  end

  create_table "menu_configs", force: :cascade do |t|
    t.string "name"
    t.text "description"
  end

  create_table "menus", force: :cascade do |t|
    t.bigint "menu_config_id", null: false
    t.bigint "grouping_id", null: false
    t.index ["grouping_id"], name: "index_menus_on_grouping_id"
    t.index ["menu_config_id"], name: "index_menus_on_menu_config_id"
  end

  create_table "pour_prices", force: :cascade do |t|
    t.bigint "beverage_id", null: false
    t.bigint "pour_size_id", null: false
    t.float "pour_price", null: false
    t.index ["beverage_id"], name: "index_pour_prices_on_beverage_id"
    t.index ["pour_size_id"], name: "index_pour_prices_on_pour_size_id"
  end

  create_table "pour_sizes", force: :cascade do |t|
    t.string "size"
  end

  create_table "producers", force: :cascade do |t|
    t.string "name"
    t.text "description"
  end

  create_table "users", id: false, force: :cascade do |t|
    t.string "id"
    t.string "token", default: ""
  end

  add_foreign_key "beverage_to_groupings", "beverages"
  add_foreign_key "beverage_to_groupings", "groupings"
  add_foreign_key "beverages", "beverage_types"
  add_foreign_key "beverages", "producers"
  add_foreign_key "kegs", "beverages"
  add_foreign_key "kegs", "keg_sizes"
  add_foreign_key "kegs", "keg_states"
  add_foreign_key "menu_config_values", "menu_config_options"
  add_foreign_key "menu_config_values", "menu_configs"
  add_foreign_key "menus", "groupings"
  add_foreign_key "menus", "menu_configs"
  add_foreign_key "pour_prices", "beverages"
  add_foreign_key "pour_prices", "pour_sizes"
end
