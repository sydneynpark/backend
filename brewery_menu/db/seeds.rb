MenuConfigOption.create!([
                             {name: "display_type", datatype: "display_type", default_value: "grid"},
                             {name: "table_header_color", datatype: "color", default_value: "#2e2e2e"},
                             {name: "rows", datatype: "integer", default_value: "4"},
                             {name: "columns", datatype: "integer", default_value: "2"},
                             {name: "use_background_image", datatype: "boolean", default_value: "false"},
                             {name: "background_image", datatype: "string", default_value: "nil"},
                             {name: "title_font", datatype: "font", default_value: "stentiga"},
                             {name: "text_font", datatype: "font", default_value: "sans-serif"},
                             {name: "title", datatype: "string", default_value: "Test Menu"},
                             {name: "include_description", datatype: "boolean", default_value: "true"},
                             {name: "include_pours", datatype: "boolean", default_value: "true"},
                             {name: "include_producers", datatype: "boolean", default_value: "true"},
                             {name: "background_color", datatype: "color", default_value: "#404040"},
                             {name: "beverage_card_color", datatype: "color", default_value: "#2e2e2e"}
                         ])