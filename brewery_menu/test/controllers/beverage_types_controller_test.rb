require 'test_helper'

class BeverageTypesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @beverage_type = beverage_types(:ipa)
  end

  test "should get beverage_types results and return 200" do
    get beverage_types_url
    assert_equal 200, status
  end

  test "creating a beverage type, returns 201" do
    post beverage_types_url, params: { type_name: "Wheat", description: "A classic brew" }
    assert_equal 201, status
  end

  test "should create beverage_type" do
    assert_difference('BeverageType.count') do
      post beverage_types_url, params: { description: @beverage_type.description, type_name: @beverage_type.type_name }, as: :json
    end

    assert_response 201
  end

  test "should show beverage_type" do
    get beverage_type_url(@beverage_type), as: :json
    assert_response :success
  end

  test "should update beverage_type" do
    patch beverage_type_url(@beverage_type), params: { description: @beverage_type.description, type_name: @beverage_type.type_name }, as: :json
    assert_response 200
  end

  test "update should change the pour size" do
    thing_to_update = beverage_types(:update_once)
    patch beverage_type_url(thing_to_update), params: { type_name: "IPA", description: "A VERY hoppy brew" }
    assert_response 200
  end

  test "should destroy beverage_type" do
    thing_to_delete = beverage_types(:delete_once)
    delete beverage_type_url(thing_to_delete)
    assert_response 204
  end

  test "get on non-existing entity should return 404" do
    thing_to_delete = beverage_types(:delete_to_get)
    delete beverage_type_url(thing_to_delete)
    get beverage_type_url(thing_to_delete)
    assert_response 404
  end
end
