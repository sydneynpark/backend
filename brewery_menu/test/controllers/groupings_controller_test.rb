require 'test_helper'

class GroupingsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @grouping = groupings(:one)
  end

  test "should get index" do
    get groupings_url, as: :json
    assert_response :success
  end

  test "should create grouping" do
    assert_difference('Grouping.count') do
      post groupings_url, params: { grouping: { name: @grouping.name } }, as: :json
    end

    assert_response 201
  end

  test "should show grouping" do
    get grouping_url(@grouping), as: :json
    assert_response :success
  end

  test "should update grouping" do
    patch grouping_url(@grouping), params: { grouping: { name: @grouping.name } }, as: :json
    assert_response 200
  end

  test "should destroy grouping" do
    assert_difference('Grouping.count', -1) do
      delete grouping_url(@grouping), as: :json
    end

    assert_response 204
  end
end
