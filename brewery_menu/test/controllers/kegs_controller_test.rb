require 'test_helper'

class KegsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @keg = kegs(:one)
  end

  test "should get index" do
    get kegs_url, as: :json
    assert_response :success
  end

  test "should create keg" do
    assert_difference('Keg.count') do
      #binding.pry
      post kegs_url, params: 
      { keg: 
        { beverage_id: beverages(:two).id, 
          keg_size_id: keg_sizes(:one).id, 
          keg_state_id: keg_states(:one).id 
        } 
      }, as: :json
    end

    assert_response 201
  end

  test "should show keg" do
    get keg_url(@keg), as: :json
    assert_response :success
  end

  test "should update keg" do
    patch keg_url(@keg), params: 
    { keg: 
      { beverage_id: @keg.beverage_id, 
        keg_size_id: @keg.keg_size_id, 
        keg_state_id: @keg.keg_state_id 
      } 
    }, as: :json
    assert_response 200
  end

  test "should destroy keg" do
    assert_difference('Keg.count', -1) do
      delete keg_url(@keg), as: :json
    end

    assert_response 204
  end
end
