require 'test_helper'

class KegStatesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @keg_state = keg_states(:one)
  end

  test "should get index" do
    get keg_states_url, as: :json
    assert_response :success
  end

  test "should create keg_state" do
    assert_difference('KegState.count') do
      post keg_states_url, params: { keg_state: { description: @keg_state.description } }, as: :json
    end

    assert_response 201
  end

  test "should show keg_state" do
    get keg_state_url(@keg_state), as: :json
    assert_response :success
  end

  test "should update keg_state" do
    patch keg_state_url(@keg_state), params: { keg_state: { description: @keg_state.description } }, as: :json
    assert_response 200
  end

  test "should destroy keg_state" do
    #NOTE: A Keg state cannot be deleted if it has a presence in the kegs table.  Therefore, because
    # Postgres automatically generates the same ids for a sequence, if the id for keg_states(:three) 
    # is somehow present in the Kegs table in the future, should another Keg be added to the kegs YAML, then this test
    # may fail in the future.
    assert_difference('KegState.count', -1) do
      delete keg_state_url(keg_states(:three)), as: :json
    end

    assert_response 204
  end
end
