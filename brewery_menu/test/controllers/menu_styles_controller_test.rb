require 'test_helper'

class MenuStylesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @menu_style = menu_styles(:one)
  end

  test "should get index" do
    get menu_styles_url, as: :json
    assert_response :success
  end

  test "should create menu_style" do
    assert_difference('MenuStyle.count') do
      post menu_styles_url, params: { menu_style: { background_color: @menu_style.background_color, background_image: @menu_style.background_image, beverage_card_color: @menu_style.beverage_card_color, columns: @menu_style.columns, include_description: @menu_style.include_description, include_pours: @menu_style.include_pours, rows: @menu_style.rows, text_font: @menu_style.text_font, title: @menu_style.title, title_font: @menu_style.title_font, use_background_image: @menu_style.use_background_image } }, as: :json
    end

    assert_response 201
  end

  test "should show menu_style" do
    get menu_style_url(@menu_style), as: :json
    assert_response :success
  end

  test "should update menu_style" do
    patch menu_style_url(@menu_style), params: { menu_style: { background_color: @menu_style.background_color, background_image: @menu_style.background_image, beverage_card_color: @menu_style.beverage_card_color, columns: @menu_style.columns, include_description: @menu_style.include_description, include_pours: @menu_style.include_pours, rows: @menu_style.rows, text_font: @menu_style.text_font, title: @menu_style.title, title_font: @menu_style.title_font, use_background_image: @menu_style.use_background_image } }, as: :json
    assert_response 200
  end

  test "should destroy menu_style" do
    assert_difference('MenuStyle.count', -1) do
      delete menu_style_url(@menu_style), as: :json
    end

    assert_response 204
  end
end
