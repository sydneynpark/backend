require 'test_helper'

class MenuConfigValuesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @menu_config_value = menu_config_values(:one)
  end

  test "should get index" do
    get menu_config_values_url, as: :json
    assert_response :success
  end

  test "should create menu_config_value" do
    assert_difference('MenuConfigValue.count') do
      post menu_config_values_url, params: { menu_config_value: { menu_config_id: @menu_config_value.menu_config_id, menu_config_option_id: @menu_config_value.menu_config_option_id, value: @menu_config_value.value } }, as: :json
    end

    assert_response 201
  end

  test "should show menu_config_value" do
    get menu_config_value_url(@menu_config_value), as: :json
    assert_response :success
  end

  test "should update menu_config_value" do
    patch menu_config_value_url(@menu_config_value), params: { menu_config_value: { menu_config_id: @menu_config_value.menu_config_id, menu_config_option_id: @menu_config_value.menu_config_option_id, value: @menu_config_value.value } }, as: :json
    assert_response 200
  end

  test "should destroy menu_config_value" do
    assert_difference('MenuConfigValue.count', -1) do
      delete menu_config_value_url(@menu_config_value), as: :json
    end

    assert_response 204
  end
end
