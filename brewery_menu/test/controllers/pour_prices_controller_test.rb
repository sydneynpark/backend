require 'test_helper'

class PourPricesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pour_price = pour_prices(:one)
  end

  test "should get index" do
    get pour_prices_url, as: :json
    assert_response :success
  end

  test "should create pour_price" do
    assert_difference('PourPrice.count') do
      post pour_prices_url, params: { pour_price: { beverage_id: @pour_price.beverage_id, pour_size_id: @pour_price.pour_size_id } }, as: :json
    end

    assert_response 201
  end

  test "should show pour_price" do
    get pour_price_url(@pour_price), as: :json
    assert_response :success
  end

  test "should update pour_price" do
    patch pour_price_url(@pour_price), params: { pour_price: { beverage_id: @pour_price.beverage_id, pour_size_id: @pour_price.pour_size_id } }, as: :json
    assert_response 200
  end

  test "should destroy pour_price" do
    assert_difference('PourPrice.count', -1) do
      delete pour_price_url(@pour_price), as: :json
    end

    assert_response 204
  end
end
