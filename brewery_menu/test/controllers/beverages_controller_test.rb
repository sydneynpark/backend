require 'test_helper'

class BeveragesControllerTest < ActionDispatch::IntegrationTest
   setup do
    @beverage_type = beverage_types(:ipa)
    @producer = producers(:one)
    @beverage = beverages(:one)
   end


  test "should beverages results and return 200" do
    get beverages_url
    assert_equal 200, status
  end

  test "should get index" do
    get beverages_url, as: :json
    assert_response :success
  end

  test "should create beverage" do
    assert_difference('Beverage.count') do
      post beverages_url, params: 
        { abv: 9.2, 
          beverage_type: @beverage_type.type_name, 
          color: 'FF00FF', 
          description: 'Sample description', 
          name: 'Test Bev for Create',
          producer_id: @producer.name
        }, as: :json
    end

    assert_response 201
  end

  test "should show beverage" do
    get beverage_url(@beverage), as: :json
    assert_response :success
  end

  test "should update beverage" do
    bev = beverages(:two)
    patch beverage_url(bev), params: 
      { abv: bev.abv, 
        beverage_type: @beverage_type.type_name, 
        color: bev.color, 
        description: bev.description, 
        name: bev.name ,
        producer_id: @producer.name
      }, as: :json
    assert_response 200
  end

  test "should destroy beverage" do
    thing_to_delete = beverages(:delete_once)
    delete beverage_url(thing_to_delete)
 
    assert_response 204
  end
end
