require 'test_helper'

class KegSizesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @keg_size = keg_sizes(:one)
  end

  test "should get index" do
    get keg_sizes_url, as: :json
    assert_response :success
  end

  test "should create keg_size" do
    assert_difference('KegSize.count') do
      post keg_sizes_url, params: { keg_size: { name: @keg_size.name } }, as: :json
    end

    assert_response 201
  end

  test "should show keg_size" do
    get keg_size_url(@keg_size), as: :json
    assert_response :success
  end

  test "should update keg_size" do
    patch keg_size_url(@keg_size), params: { keg_size: { name: @keg_size.name } }, as: :json
    assert_response 200
  end

  test "should destroy keg_size" do
    #NOTE: A Keg state cannot be deleted if it has a presence in the kegs table.  Therefore, because
    # Postgres automatically generates the same ids for a sequence, if the id for keg_states(:three) 
    # is somehow present in the Kegs table in the future, should another Keg be added to the kegs YAML, then this test
    # may fail in the future.
    assert_difference('KegSize.count', -1) do
      delete keg_size_url(keg_sizes(:three)), as: :json
    end

    assert_response 204
  end
end
