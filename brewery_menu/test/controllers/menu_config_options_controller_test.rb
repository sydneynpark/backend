require 'test_helper'

class MenuConfigOptionsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @menu_config_option = menu_config_options(:one)
  end

  test "should get index" do
    get menu_config_options_url, as: :json
    assert_response :success
  end

  test "should create menu_config_option" do
    assert_difference('MenuConfigOption.count') do
      post menu_config_options_url, params: { menu_config_option: { datatype: @menu_config_option.datatype, default_value: @menu_config_option.default_value, name: @menu_config_option.name } }, as: :json
    end

    assert_response 201
  end

  test "should show menu_config_option" do
    get menu_config_option_url(@menu_config_option), as: :json
    assert_response :success
  end

  test "should update menu_config_option" do
    patch menu_config_option_url(@menu_config_option), params: { menu_config_option: { datatype: @menu_config_option.datatype, default_value: @menu_config_option.default_value, name: @menu_config_option.name } }, as: :json
    assert_response 200
  end

  test "should destroy menu_config_option" do
    assert_difference('MenuConfigOption.count', -1) do
      delete menu_config_option_url(@menu_config_option), as: :json
    end

    assert_response 204
  end
end
