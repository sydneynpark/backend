require 'test_helper'

class MenuConfigsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @menu_config = menu_configs(:one)
  end

  test "should get index" do
    get menu_configs_url, as: :json
    assert_response :success
  end

  test "should create menu_config" do
    assert_difference('MenuConfig.count') do
      post menu_configs_url, params: { menu_config: { description: @menu_config.description, name: @menu_config.name } }, as: :json
    end

    assert_response 201
  end

  test "should show menu_config" do
    get menu_config_url(@menu_config), as: :json
    assert_response :success
  end

  test "should update menu_config" do
    patch menu_config_url(@menu_config), params: { menu_config: { description: @menu_config.description, name: @menu_config.name } }, as: :json
    assert_response 200
  end

  test "should destroy menu_config" do
    assert_difference('MenuConfig.count', -1) do
      delete menu_config_url(@menu_config), as: :json
    end

    assert_response 204
  end
end
