require 'test_helper'

class PourSizesControllerTest < ActionDispatch::IntegrationTest
  setup do
    @pour_size = pour_sizes(:four)
  end

  test "getting all pour sizes should return 200" do
    get pour_sizes_url
    assert_equal 200, status
  end

  test "creating a pour size should return 201" do
    post pour_sizes_url, params: { pour_size: { size: '4-pack to go'} }
    assert_equal  201, status
  end

  test "creating a pour size without a name should fail" do
    post pour_sizes_url, params: { pour_size: { } }

    # Make sure we get a response of "422 Unprocessable Entity"
    assert_equal 422, status
  end

  test "should create pour_size" do
    # This is the auto-generated test
    assert_difference('PourSize.count') do
      post pour_sizes_url, params: { pour_size: { size: @pour_size.size } }, as: :json
    end

    assert_response 201
  end

  test "should show pour_size" do
    get pour_size_url(@pour_size), as: :json
    assert_response :success
  end

  test "should update pour_size" do
    patch pour_size_url(@pour_size), params: { pour_size: { size: @pour_size.size } }, as: :json
    assert_response 200
  end

  test "should destroy pour_size" do

    # post pour_sizes_url, params: { pour_size: { size: 'growler'} }
    # @response.parsed_body["id"]
    # delete pour_sizes_url(@response.parsed_body)
    # assert_response 204

    thing_to_delete = pour_sizes(:delete_once)
    delete pour_size_url(thing_to_delete)
    assert_response 204

  end

  test "update should change the pour size" do
    thing_to_update = pour_sizes(:update_once)
    patch pour_size_url(thing_to_update), params: { pour_size: { size: 'New description' } }
    assert_response 200
  end

  test "get on non-existing entity should return 404" do
    thing_to_delete = pour_sizes(:delete_to_get)
    delete pour_size_url(thing_to_delete)
    get pour_size_url(thing_to_delete)
    assert_response 404
  end

end
