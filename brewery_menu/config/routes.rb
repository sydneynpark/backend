Rails.application.routes.draw do
  scope 'api' do

    get '/menu_configs/with_values/:id', to: 'menu_configs#show_with_values'
    get '/menu_configs/with_values', to: 'menu_configs#index_with_values'

    resources :menus
    resources :menu_config_values
    resources :menu_configs
    resources :menu_config_options
    resources :menu_styles
    post '/users/login', to: 'users#login'
    post '/users/create', to: 'users#create'

    get '/bev_names_for_group/:group', to: 'beverage_to_groupings#getBevNamesForGroup'
    get '/enhanced_beverages', to: 'beverages#enhancedBeverages'
    post '/uploads/parse', to: 'uploads#parseFile'

    resources :groupings
    resources :kegs
    resources :keg_states
    resources :keg_sizes
    get '/pour_prices/prices', to: 'pour_prices#prices'
    resources :pour_prices
    resources :pour_sizes
    resources :beverages
    resources :beverage_types
    resources :producers
    resources :beverage_to_groupings
    # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
    get 'backup/create'
    post 'backup/load'
  end
end
